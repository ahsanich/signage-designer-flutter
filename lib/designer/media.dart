import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:signage_designer/designer/foundation.dart';

class ImageWidget extends FrameWidget {
  ImageWidget(String id, Offset pPosition, Offset pSize)
      : super(id, pPosition: pPosition, pSize: pSize);

  @override
  State<StatefulWidget> createState() => _ImageWidgetState();
}

class _ImageWidgetState extends FrameWidgetState<ImageWidget> {
  @override
  Widget buildLayout(BuildContext context, Size size) {
    return Image.asset('images/img1.jpg');
  }
}
