import 'dart:ui';

mixin WidgetProperty {
  static const String FIELD_POSITION_X = "posX";
  static const String FIELD_POSITION_Y = "posY";
  static const String FIELD_WIDTH = "width";
  static const String FIELD_HEIGHT = "height";

  Offset pSize = Offset(0.5, 0.5), pPosition = Offset(0, 0);
}
