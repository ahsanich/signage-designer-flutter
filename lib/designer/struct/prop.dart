import 'package:flutter/material.dart';

// class Prop {
//   static final String FIELD_ID = "id";
//   static final String FIELD_POSITION_X = "posX";
//   static final String FIELD_POSITION_Y = "posY";
//   static final String FIELD_WIDTH = "width";
//   static final String FIELD_HEIGHT = "height";

//   String id;
//   Offset position;
//   Size size;
//   final Size parentSize;

//   /*
//    ******************************** Getters ************************************
//    */

//   double get pX => _validate(position.dx / parentSize.width);

//   double get pY => _validate(position.dy / parentSize.height);

//   double get pW => _validate(size.width / parentSize.width);

//   double get pH => _validate(size.height / parentSize.height);

//   /*
//    ****************************** Constructor **********************************
//    */

//   // TODO - Count params supplied in the constructor
//   Prop(this.parentSize, Map map) {
//     double _pX = map[FIELD_POSITION_X];
//     double _pY = map[FIELD_POSITION_Y];
//     double _pW = map[FIELD_WIDTH];
//     double _pH = map[FIELD_HEIGHT];

//     id = map[FIELD_ID];
//     position = Offset(_pX * parentSize.width, _pY * parentSize.height);
//     size = Size(_pW * parentSize.width, _pH * parentSize.height);
//   }

//   /*
//    ******************************** Methods ************************************
//    */

//   Prop clone() {
//     return Prop(parentSize, {
//       FIELD_ID: id,
//       FIELD_POSITION_X: pX,
//       FIELD_POSITION_Y: pY,
//       FIELD_WIDTH: pW,
//       FIELD_HEIGHT: pH
//     });
//   }

//   bool equals(Prop p) {
//     if (id != p.id) return false;
//     return equateProp(p);
//   }

//   bool equateProp(Prop p) {
//     return position.dx == p.position.dx &&
//         position.dy == p.position.dy &&
//         size.width == p.size.width &&
//         size.height == p.size.height;
//   }

//   String details() => "id : $id | pos : $position | size : $size";

//   double _validate(double value) {
//     if (value < 0)
//       return 0.0;
//     else if (value > 1)
//       return 1.0;
//     else
//       return value;
//   }
// }
