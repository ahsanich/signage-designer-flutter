class Helper {
  static double min(double a, double b) {
    return a < b ? (a < 0 ? 0 : a) : (b < 0 ? 0 : b);
  }
}
