import 'package:flutter/material.dart';
import 'package:signage_designer/designer/foundation.dart';
import 'package:signage_designer/designer/space.dart';

class Design extends StatefulWidget {
  final Map fakelate;

  Design(this.fakelate);

  @override
  State<StatefulWidget> createState() => _DesignState();
}

class _DesignState extends State<Design> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraint) {
        String id = widget.fakelate[FrameWidget.FIELD_ID];
        bool isLeaf = widget.fakelate[SpaceWidget.FIELD_IS_LEAF];
        List<Map> children = widget.fakelate[SpaceWidget.FIELD_CHILDREN];
        print("Details => $id");
        return SpaceWidget(id, isLeaf, children);
      },
    );
  }
}
