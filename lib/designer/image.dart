import 'package:flutter/material.dart';
import 'package:signage_designer/designer/struct/prop.dart';

import 'foundation.dart';

// class ImageMedia extends StatefulWidget {
//   final Prop prop;
//   final Size pSize;
//   final String asset;

//   ImageMedia(this.prop, this.pSize, {this.asset = 'images/img1.jpg'});

//   @override
//   State<StatefulWidget> createState() => _ImageMedia();
// }

// class _ImageMedia extends State<ImageMedia> {
//   @override
//   Widget build(BuildContext context) {
//     Prop prop = widget.prop;
//     return GestureDetector(
//       onPanUpdate: (drag) => setState(() => prop.position += drag.delta),
//       child: CustomSingleChildLayout(
//           delegate: MediaLayoutDelegate(prop.position),
//           child: Image.asset(
//             widget.asset,
//             width: prop.size.width,
//             height: prop.size.height,
//           )),
//     );
//   }
// }
