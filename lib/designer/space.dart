import 'dart:developer';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:signage_designer/designer/helper.dart';
import 'package:signage_designer/designer/media.dart';
import 'package:signage_designer/designer/struct/prop.dart';
import 'package:signage_designer/designer/foundation.dart';
import 'package:signage_designer/designer/struct/widget_structure.dart';

class SpaceWidget extends FrameWidget {
  static const String FIELD_IS_LEAF = "leaf";
  static const String FIELD_CHILDREN = "children";

  bool isLeaf;
  final List<FrameWidget> children = List();

/*
 *************************   Constructor   *************************
 */

  SpaceWidget(String id, this.isLeaf, List<Map> childMaps,
      {Offset pPosition = const Offset(0, 0),
      Offset pSize = const Offset(1, 1)})
      : super(id, pPosition: pPosition, pSize: pSize) {
    childMaps.forEach((map) {
      String id = map[FrameWidget.FIELD_ID];
      double dX = map[WidgetProperty.FIELD_POSITION_X];
      double dY = map[WidgetProperty.FIELD_POSITION_Y];
      double dW = map[WidgetProperty.FIELD_WIDTH];
      double dH = map[WidgetProperty.FIELD_HEIGHT];
      bool isLeaf = map[FIELD_IS_LEAF];

      Offset pPosition = Offset(dX, dY);
      Offset pSize = Offset(dW, dH);

      List<dynamic> grandChildMaps = map[FIELD_CHILDREN];

      if (grandChildMaps != null /** && grandChildMaps.isNotEmpty */) {
        FrameWidget frameWidget;
        if (isLeaf) {
          frameWidget = new ImageWidget(id, pPosition, pSize);
        } else {
          frameWidget = new SpaceWidget(id, isLeaf, grandChildMaps,
              pPosition: pPosition, pSize: pSize);
        }
        print("Details => ${super.id} => $id : $frameWidget");
        children.add(frameWidget);
      }
    });
  }

  @override
  State<StatefulWidget> createState() => _SpaceWidgetState();
}

class _SpaceWidgetState extends FrameWidgetState<SpaceWidget> {
  @override
  Widget buildLayout(BuildContext context, Size size) {
    return CustomMultiChildLayout(
        delegate: SpaceLayoutDelegate(widget.children),
        children: _buildChildrenLayout(size));
  }

  List<Widget> _buildChildrenLayout(Size size) {
    List<Widget> widgets = new List();

    widget.children.forEach((FrameWidget frameWidget) {
      Widget _childWidget;
      if ((frameWidget is SpaceWidget && frameWidget.children.isNotEmpty) ||
          (frameWidget is ImageWidget)) {
        _childWidget = frameWidget;
      }

      if (_childWidget == null) return;

      // _SpaceWidgetContainer _spaceContainer = _SpaceWidgetContainer(childProp, child);
      
      _childWidget = Container(
          color: Colors
              .transparent, // TODO - Remove this property; this is only to make gesture detector work
          width: size.width * frameWidget.pSize.dx,
          height: size.height * frameWidget.pSize.dy,
          child: _childWidget);

      GestureDetector _gestureDetector = GestureDetector(
          onPanUpdate: (drag) => setState(() {
                Offset newPos = frameWidget.position + drag.delta;
                double dX =
                    Helper.min(newPos.dx, size.width - frameWidget.size.width);
                double dY = Helper.min(
                    newPos.dy, size.height - frameWidget.size.height);
                newPos = Offset(dX, dY);
                frameWidget.position = newPos;
              }),
          child: _childWidget);

      LayoutId layout = LayoutId(id: frameWidget.id, child: _gestureDetector);

      widgets.add(layout);
    });
    return widgets;
  }
}

/*
 ******************************** Space Container ******************************
      ******************************** TODO  ******************************
 */

// class _SpaceWidgetContainer extends StatefulWidget {
//   final Prop _prop;
//   final Widget _child;

//   _SpaceWidgetContainer(this._prop, this._child);

//   @override
//   State<StatefulWidget> createState() {
//     return _SpaceWidgetContainerState();
//   }
// }

// class _SpaceWidgetContainerState extends State<_SpaceWidgetContainer> {
//   BoxDecoration boarderBox;

//   @override
//   Widget build(BuildContext context) {
//     Prop prop = widget._prop;
//     Widget child = widget._child;

//     Container _container = Container(
//         width: prop.size.width,
//         height: prop.size.height,
//         decoration: boarderBox,
//         child: child);

//     return Listener(
//       child: _container,
//       onPointerMove: (event) {
//         if (boarderBox == null) setState(() => showBoader());
//       },
//       onPointerHover: (event) {
//         if (boarderBox == null) setState(() => showBoader());
//       },
//       onPointerExit: (event) {
//         setState(() => hideBoader());
//       },
//       onPointerUp: (event) {
//         setState(() => hideBoader());
//       },
//     );
//   }

//   void showBoader() {
//     Random rng = new Random();
//     boarderBox = BoxDecoration(
//         border: Border.all(
//             color: Color.fromARGB(
//                 255, rng.nextInt(128), rng.nextInt(128), rng.nextInt(128))));
//   }

//   void hideBoader() {
//     boarderBox = null;
//   }
// }
