import 'package:flutter/material.dart';
import 'package:signage_designer/designer/helper.dart';
import 'package:signage_designer/designer/struct/widget_structure.dart';

abstract class FrameWidget extends StatefulWidget with WidgetProperty {
  static const String FIELD_ID = "id";
  static const String FIELD_WIDGET_PROPERTY = "widget_property";

  final String id;
  Offset position;
  Size size;

  FrameWidget(this.id,
      {Offset pSize = const Offset(0.5, 0.5),
      Offset pPosition = const Offset(0, 0)}) {
    super.pSize = pSize;
    super.pPosition = pPosition;
    position = Offset(0, 0); // TODO - Set based on pPosition
    size = Size(200, 200);
  }
}

abstract class FrameWidgetState<T extends FrameWidget> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      widget.size = Size(constraint.maxWidth, constraint.maxHeight);
      return buildLayout(context, widget.size);
    });
  }

  Widget buildLayout(BuildContext context, Size size);
}
/*
 *********************** Delegate layout implementation ************************
 */

class MediaLayoutDelegate extends SingleChildLayoutDelegate {
  Offset _position;

  MediaLayoutDelegate(this._position);

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    return constraints.loosen();
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    return _position;
  }

  @override
  bool shouldRelayout(MediaLayoutDelegate oldDelegate) {
    return _position != oldDelegate._position;
  }
}

/*
 *******************************************************************************
 */

// class SpaceLayoutDelegate extends MultiChildLayoutDelegate {
//   int _size;
//   List<Prop> _props;

//   SpaceLayoutDelegate(List<Space> spaces) {
//     this._size = 0;
//     this._props = spaces.map((space) {
//       _size++;
//       return space.prop.clone();
//     }).toList();
//   }

//   @override
//   bool shouldRelayout(SpaceLayoutDelegate oldDelegate) {
//     if (oldDelegate._size != _size) return true;
//     for (Prop oldItem in oldDelegate._props) {
//       for (Prop newItem in _props) {
//         if (oldItem.id == newItem.id && !oldItem.equateProp(newItem))
//           return true;
//       }
//     }
//     return false;
//   }

//   @override
//   void performLayout(Size size) {
//     _props.forEach((prop) {
//       if (hasChild(prop.id)) {
//         Offset pos = prop.position;
//         pos = Offset(Helper.min(pos.dx, size.width - prop.size.width),
//             Helper.min(pos.dy, size.height - prop.size.height));
//         layoutChild(prop.id, BoxConstraints.loose(size));
//         positionChild(prop.id, pos);
//       }
//     });
//   }
// }

class SpaceLayoutDelegate extends MultiChildLayoutDelegate {
  // int _size;
  List<FrameWidget> frameList;

  SpaceLayoutDelegate(this.frameList);

  @override
  bool shouldRelayout(SpaceLayoutDelegate oldDelegate) {
    return true;
    // if (oldDelegate._size != _size) return true;
    // for (Prop oldItem in oldDelegate._props) {
    //   for (Prop newItem in _props) {
    //     if (oldItem.id == newItem.id && !oldItem.equateProp(newItem))
    //       return true;
    //   }
    // }
    // return false;
  }

  @override
  void performLayout(Size size) {
    frameList.forEach((frame) {
      if (hasChild(frame.id)) {
        Offset pos = frame.position;
        pos = Offset(Helper.min(pos.dx, size.width - frame.size.width),
            Helper.min(pos.dy, size.height - frame.size.height));
        layoutChild(frame.id, BoxConstraints.loose(size));
        positionChild(frame.id, pos);
      }
    });
  }
}

/*
 ******************** END - Delegate layout implementation *********************
 */
