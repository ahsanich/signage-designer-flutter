import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'designer/design.dart';
import 'designer/foundation.dart';
import 'designer/space.dart';
import 'designer/struct/prop.dart';
import 'designer/struct/widget_structure.dart';

void main() {
  debugPaintSizeEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Design(fakelate());
  }

  Map fakelate() {
    return {
      FrameWidget.FIELD_ID: 'root',
      SpaceWidget.FIELD_IS_LEAF: false,
      WidgetProperty.FIELD_WIDTH: 1,
      WidgetProperty.FIELD_HEIGHT: 1,
      WidgetProperty.FIELD_POSITION_X: 0,
      WidgetProperty.FIELD_POSITION_Y: 0,
      SpaceWidget.FIELD_CHILDREN: [
        {
          FrameWidget.FIELD_ID: 'child_1',
          SpaceWidget.FIELD_IS_LEAF: false,
          WidgetProperty.FIELD_WIDTH: 0.5,
          WidgetProperty.FIELD_HEIGHT: 0.5,
          WidgetProperty.FIELD_POSITION_X: 0.5,
          WidgetProperty.FIELD_POSITION_Y: 0.5,
          SpaceWidget.FIELD_CHILDREN: [
            {
              FrameWidget.FIELD_ID: 'child_3',
              SpaceWidget.FIELD_IS_LEAF: true,
              WidgetProperty.FIELD_WIDTH: 0.5,
              WidgetProperty.FIELD_HEIGHT: 0.5,
              WidgetProperty.FIELD_POSITION_X: 0,
              WidgetProperty.FIELD_POSITION_Y: 0,
              SpaceWidget.FIELD_CHILDREN: []
            },
            {
              FrameWidget.FIELD_ID: 'child_4',
              SpaceWidget.FIELD_IS_LEAF: true,
              WidgetProperty.FIELD_WIDTH: 0.5,
              WidgetProperty.FIELD_HEIGHT: 0.5,
              WidgetProperty.FIELD_POSITION_X: 0,
              WidgetProperty.FIELD_POSITION_Y: 0,
              SpaceWidget.FIELD_CHILDREN: []
            },
            {
              FrameWidget.FIELD_ID: 'child_5',
              SpaceWidget.FIELD_IS_LEAF: true,
              WidgetProperty.FIELD_WIDTH: 0.5,
              WidgetProperty.FIELD_HEIGHT: 0.5,
              WidgetProperty.FIELD_POSITION_X: 0,
              WidgetProperty.FIELD_POSITION_Y: 0,
              SpaceWidget.FIELD_CHILDREN: []
            },
            {
              FrameWidget.FIELD_ID: 'child_6',
              SpaceWidget.FIELD_IS_LEAF: false,
              WidgetProperty.FIELD_WIDTH: 0.5,
              WidgetProperty.FIELD_HEIGHT: 0.5,
              WidgetProperty.FIELD_POSITION_X: 0,
              WidgetProperty.FIELD_POSITION_Y: 0,
              SpaceWidget.FIELD_CHILDREN: [
                {
                  FrameWidget.FIELD_ID: 'child_8',
                  SpaceWidget.FIELD_IS_LEAF: true,
                  WidgetProperty.FIELD_WIDTH: 0.5,
                  WidgetProperty.FIELD_HEIGHT: 0.5,
                  WidgetProperty.FIELD_POSITION_X: 0,
                  WidgetProperty.FIELD_POSITION_Y: 0,
                  SpaceWidget.FIELD_CHILDREN: []
                },
                {
                  FrameWidget.FIELD_ID: 'child_9',
                  SpaceWidget.FIELD_IS_LEAF: true,
                  WidgetProperty.FIELD_WIDTH: 0.5,
                  WidgetProperty.FIELD_HEIGHT: 0.5,
                  WidgetProperty.FIELD_POSITION_X: 0,
                  WidgetProperty.FIELD_POSITION_Y: 0,
                  SpaceWidget.FIELD_CHILDREN: []
                },
                {
                  FrameWidget.FIELD_ID: 'child_10',
                  SpaceWidget.FIELD_IS_LEAF: true,
                  WidgetProperty.FIELD_WIDTH: 0.5,
                  WidgetProperty.FIELD_HEIGHT: 0.5,
                  WidgetProperty.FIELD_POSITION_X: 0,
                  WidgetProperty.FIELD_POSITION_Y: 0,
                  SpaceWidget.FIELD_CHILDREN: []
                },
                {
                  FrameWidget.FIELD_ID: 'child_11',
                  SpaceWidget.FIELD_IS_LEAF: true,
                  WidgetProperty.FIELD_WIDTH: 0.5,
                  WidgetProperty.FIELD_HEIGHT: 0.5,
                  WidgetProperty.FIELD_POSITION_X: 0,
                  WidgetProperty.FIELD_POSITION_Y: 0,
                  SpaceWidget.FIELD_CHILDREN: []
                }
              ]
            }
          ]
        },
        {
          FrameWidget.FIELD_ID: 'child_2',
          SpaceWidget.FIELD_IS_LEAF: true,
          WidgetProperty.FIELD_WIDTH: 0.4 / 2.1,
          WidgetProperty.FIELD_HEIGHT: 0.4,
          WidgetProperty.FIELD_POSITION_X: 0,
          WidgetProperty.FIELD_POSITION_Y: 0,
          SpaceWidget.FIELD_CHILDREN: []
        }
      ]
    };
  }
}
